package com.oracle.ws.client;

import java.net.HttpURLConnection;
import java.net.URL;

public class test {
	
	public static void main (String[] args){
		
		boolean status = getStatus("https://hdes-test.hcm.us2.oraclecloud.com/hcmEmploymentCoreWorkerV2/WorkerService?WSDL");
		 
        System.out.println("disponible:" + status);
	
	}
	
	public static boolean getStatus(String url) {
		 
       boolean disponible = false;
        try {
            URL siteURL = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) siteURL.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
 
            int code = connection.getResponseCode();
            if (code == 200) {
            	disponible = true;
            }
        } catch (Exception e) {
        	disponible = false;
        }
        return disponible;
    }

}
