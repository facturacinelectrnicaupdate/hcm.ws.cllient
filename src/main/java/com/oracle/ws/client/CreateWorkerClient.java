package com.oracle.ws.client;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceException;

import org.apache.log4j.Logger;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

import com.oracle.ws.handlers.WSSESOAPHandler;
import com.oracle.ws.handlers.WSSESOAPHandlerResolver;
import com.oracle.xmlns.apps.hcm.employment.core.flex.baseworkerassignmentdff.BaseWorkerAsgDFF;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.ActionsList;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.Assignment;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.Person;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.PersonAddress;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.PersonDetails;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.PersonDriversLicence;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.PersonEmail;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.PersonLegislativeData;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.PersonName;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.PersonNationalIdentifier;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.PersonPhone;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.PersonUserInformation;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.WorkRelationship;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.WorkTerms;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.Worker;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.WorkerService;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.types.CreateWorkerResponse;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.types.GetWorkerInformationByPersonNumberResponse;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.types.MergePersonResponse;
import com.oracle.xmlns.apps.hcm.employment.core.workerservicev2.types.UpdateAssignmentResponse;
import com.oracle.xmlns.apps.hcm.people.core.flex.driverslicensetypedff.DriversLicenseTypeDFF;




import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Properties;


public class CreateWorkerClient {
	
	
	private static final Logger LOGGER = Logger.getLogger(CreateWorkerClient.class);
	DateTimeZone dateTimeZone = DateTimeZone.forID(DateTimeZone.getDefault().getID());
	private Properties properties;
	
	//atributos JDBC
	 Connection cn = null;
	 PreparedStatement ps = null;
	 ResultSet rs = null;
	 
	 
	 
	public CreateWorkerClient() {
		super();
		initPropertyFile();
		
		
	}



	public void createWorker(){
		
	
		//atributos para web service
		WSSESOAPHandler wsse = null;
		WSSESOAPHandlerResolver wsseHR = null;		
		CreateWorkerResponse response = null;
		MergePersonResponse mergeResponse = null;
		GetWorkerInformationByPersonNumberResponse informationResponse= null;
		UpdateAssignmentResponse updateAssignmentResponse = null;

		
		int id_number = 0;
		String metodo = "";
		String xmlGenerado1 = null, xmlGenerado2 = null, xmlGenerado3 = null;
		
		try {
		
				Class.forName(properties.getProperty("db.driver"));
				cn = DriverManager.getConnection(properties.getProperty("db.url"), properties.getProperty("db.user"), properties.getProperty("db.password"));
				
				String sql = "SELECT id_number, no_persona, fecha_contratacion, fecha_inicio, fecha_nacimiento," 
							+"tipo_sangre, accion, codigo_legislacion, sexo, estado_civil," 
							+"apellido_paterno, apellido_materno, nombre, segundo_nombre, correo_empresa," 
							+"correo_personal, tipo_direccion, pais, direccion, telefono_particular1," 
					        +"telefono_particular2, telefono_particular3, movil_particular1," 
					        +"movil_particular2, movil_particular3, tipo_identificador1, numero_identificador1," 
					        +"tipo_identificador2, numero_identificador2, fecha_licencia1," 
					        +"categoria_licencia1, fecha_licencia2, categoria_licencia2, fecha_licencia3," 
					        +"categoria_licencia3, entidad_legal, tipo_trabajador, nombre_asignacion," 
					        +"departamento, codigo_posicion, unidad_negocio, nombre_banco," 
					        +"cuenta_bco, tipo_cuenta_bco, cuenta_cliente_bco, centro_funcional_dep," 
					        +"centro_funcional_cont, usuario, envia_credenciales, estado, metodo_ws," 
					        +"fecha_registro, fecha_procesamiento, respuesta_ws, flag_status," 
					        +"xml_enviado1, xml_enviado2, xml_enviado3"
					        +" FROM hcm_colaboradores where estado in (?, ?)";

				ps = cn.prepareStatement(sql); 
		   		ps.setString(1, "C");
		   		ps.setString(2, "U");
		   		rs = ps.executeQuery();
		   		
		   		Worker w = null;
				ActionsList al =null;
				
				
				
		   		while(rs.next()){
		   			
		   			if(rs.getString("estado").equals("C"))
		   			{	
		   					LOGGER.info("Proceso de creacion de un trabajador");
		   					
		   					response = new CreateWorkerResponse();
		   					
							wsse = new WSSESOAPHandler();
							wsse.setWSSE(properties.getProperty("ws.user"), properties.getProperty("ws.password"));
							wsseHR = new WSSESOAPHandlerResolver(wsse);
							Service service = Service.create(new URL(properties.getProperty("ws.endpoint")), new QName(properties.getProperty("ws.qname"), properties.getProperty("ws.name")));
							service.setHandlerResolver(wsseHR);				
							WorkerService port = service.getPort(WorkerService.class);
							
							
							id_number = rs.getInt("id_number");
							w = new Worker();
							al = new ActionsList();
							
							LOGGER.info("Obteniendo datos del trabajador: " +rs.getString("nombre"));
							
							//RangeStartDate
							w.setRangeStartDate(DocumentUtil.getXMLGregorianCalendar("RangeStartDate", rs.getString("fecha_contratacion")));
							
							//PersonNumber
							w.setPersonNumber(DocumentUtil.getXMLString("PersonNumber", rs.getString("no_persona")));
								
							//StartDate
							w.setStartDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_contratacion")));
							
							//DateOfBirth
							w.setDateOfBirth(DocumentUtil.getXMLGregorianCalendar("DateOfBirth", rs.getString("fecha_nacimiento")));		
							
							//BloodType
							w.setBloodType(DocumentUtil.getXMLString("BloodType", rs.getString("tipo_sangre")));
							
						
							//Inicio - WorkRelationship
							WorkRelationship workRelationship = new WorkRelationship();				
								workRelationship.setDateStart(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_contratacion")));
								workRelationship.setLegalEmployerName(DocumentUtil.getXMLString("LegalEmployerName", rs.getString("entidad_legal")));
								workRelationship.setWorkerType(rs.getString("tipo_trabajador"));//cambiale a otro valor y bota exception
								
								//Inicio - WorkTerms
								WorkTerms workTerms = new WorkTerms();					
									workTerms.setBusinessUnitShortCode(DocumentUtil.getXMLString("BusinessUnitShortCode", rs.getString("unidad_negocio")));
									workTerms.setRangeStartDate(DocumentUtil.getXMLGregorianCalendar("RangeStartDate", rs.getString("fecha_contratacion")));
									
									//Inicio - Assignment
									Assignment assignment = new Assignment();
									assignment.setBusinessUnitShortCode(DocumentUtil.getXMLString("BusinessUnitShortCode", rs.getString("unidad_negocio")));
									assignment.setRangeStartDate(DocumentUtil.getXMLGregorianCalendar("RangeStartDate", rs.getString("fecha_contratacion")));
									assignment.setAssignmentName(DocumentUtil.getXMLString("AssignmentName", rs.getString("nombre_asignacion")));
									assignment.setDepartmentName(DocumentUtil.getXMLString("DepartmentName", rs.getString("departamento")));
									assignment.setPositionCode(DocumentUtil.getXMLString("PositionCode", rs.getString("codigo_posicion")));
									
									//Inicio - BaseWorkerAsgDFF
									BaseWorkerAsgDFF baseWorkerAsgDFF = new BaseWorkerAsgDFF();
										baseWorkerAsgDFF.setBanco(DocumentUtil.getXMLStringBas("banco", rs.getString("nombre_banco")));
										baseWorkerAsgDFF.setCuenta(DocumentUtil.getXMLStringBas("cuenta", rs.getString("cuenta_bco")));
										baseWorkerAsgDFF.setTipoCuenta(DocumentUtil.getXMLStringBas("tipoCuenta", rs.getString("tipo_cuenta_bco")));
										baseWorkerAsgDFF.setCuentaCliente(DocumentUtil.getXMLStringBas("cuentaCliente", rs.getString("cuenta_cliente_bco")));
										baseWorkerAsgDFF.setCentroFuncionalDepartamento(DocumentUtil.getXMLStringBas("centroFuncionalDepartamento", rs.getString("centro_funcional_dep")));
										baseWorkerAsgDFF.setCentroFuncionalContable(DocumentUtil.getXMLStringBas("centroFuncionalContable", rs.getString("centro_funcional_cont")));
									workTerms.setBaseWorkerAsgDFF(baseWorkerAsgDFF);						
									//final - BaseWorkerAsgDFF	
									
									workTerms.getAssignment().add(assignment);						
									//Final - Assignment
									
								workRelationship.getWorkTerms().add(workTerms);						
								//Final - WorkTerms
								
							w.getWorkRelationship().add(workRelationship);
							//Final - WorkRelationship
							
							
							//Inicio - PersonLegislativeData
							PersonLegislativeData personLegislativeData = new PersonLegislativeData();
								personLegislativeData.setRangeStartDate(DocumentUtil.getXMLGregorianCalendar("RangeStartDate", rs.getString("fecha_contratacion")));
								personLegislativeData.setLegislationCode(rs.getString("codigo_legislacion"));
								personLegislativeData.setSex(DocumentUtil.getXMLString("Sex", rs.getString("sexo")));
								personLegislativeData.setMaritalStatus(DocumentUtil.getXMLString("MaritalStatus", rs.getString("estado_civil")));
							w.getWorkerLegislativeData().add(personLegislativeData);
							//Final - PersonLegislativeData
							
							//Inicio - WorkerEmail
							//correo de trabajo
							PersonEmail personEmailTrabajo = new PersonEmail();
							personEmailTrabajo.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_contratacion")));
							personEmailTrabajo.setEmailType("W1");
							personEmailTrabajo.setEmailAddress(rs.getString("correo_empresa"));
							
							//correo particular
							PersonEmail personEmailParticular = new PersonEmail();
							personEmailParticular.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_contratacion")));
							personEmailParticular.setEmailType("H1");
							personEmailParticular.setEmailAddress(rs.getString("correo_personal"));
							
							w.getWorkerEmail().add(personEmailTrabajo);
							w.getWorkerEmail().add(personEmailParticular);				
							//Final - WorkerEmail
							
							//Inicio - WorkerAddress
							PersonAddress personAddress = new PersonAddress();
							personAddress.setRangeStartDate(DocumentUtil.getXMLGregorianCalendar("RangeStartDate", rs.getString("fecha_contratacion")));
							personAddress.setAddressType(rs.getString("tipo_direccion"));
							personAddress.setCountry(rs.getString("pais"));
							personAddress.setAddressLine1(DocumentUtil.getXMLString("AddressLine1", rs.getString("direccion")));
							w.getWorkerAddress().add(personAddress);
							//Final - WorkerAddress
							
							//Inicio - WorkerPhone
							PersonPhone personPhone1 = new PersonPhone();
							personPhone1.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_contratacion")));
							personPhone1.setPhoneType("H1");
							personPhone1.setPhoneNumber(rs.getString("telefono_particular1"));
							
							PersonPhone personPhone2 = new PersonPhone();
							personPhone2.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_contratacion")));
							personPhone2.setPhoneType("H2");
							personPhone2.setPhoneNumber(rs.getString("telefono_particular2"));
							
							PersonPhone personPhone3 = new PersonPhone();
							personPhone3.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_contratacion")));
							personPhone3.setPhoneType("H3");
							personPhone3.setPhoneNumber(rs.getString("telefono_particular3"));
							
							PersonPhone personPhone4 = new PersonPhone();
							personPhone4.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_contratacion")));
							personPhone4.setPhoneType("HM");
							personPhone4.setPhoneNumber(rs.getString("movil_particular1"));
							
							PersonPhone personPhone5 = new PersonPhone();
							personPhone5.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_contratacion")));
							personPhone5.setPhoneType("HM2");
							personPhone5.setPhoneNumber(rs.getString("movil_particular2"));
							
							PersonPhone personPhone6 = new PersonPhone();
							personPhone6.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_contratacion")));
							personPhone6.setPhoneType("HM3");
							personPhone6.setPhoneNumber(rs.getString("movil_particular3"));
									
							w.getWorkerPhone().add(personPhone1);
							w.getWorkerPhone().add(personPhone2);
							w.getWorkerPhone().add(personPhone3);
							w.getWorkerPhone().add(personPhone4);
							w.getWorkerPhone().add(personPhone5);
							w.getWorkerPhone().add(personPhone6);
							//Final - WorkerPhone
							
							//Inicio - WorkerName
							PersonName personName = new PersonName();
							personName.setRangeStartDate(DocumentUtil.getXMLGregorianCalendar("RangeStartDate", rs.getString("fecha_contratacion")));
							personName.setNameType("GLOBAL");
							personName.setFirstName(DocumentUtil.getXMLString("FirstName", rs.getString("nombre")));
							personName.setMiddleNames(DocumentUtil.getXMLString("MiddleNames", rs.getString("segundo_nombre")));
							personName.setLastName(rs.getString("apellido_paterno"));
							personName.setNameInformation1(DocumentUtil.getXMLString("NameInformation1", rs.getString("apellido_materno")));
							personName.setTitle(DocumentUtil.getXMLString("Title", "MR."));							
							w.getWorkerName().add(personName);				
							//Final - WorkerName
							
							
							//Inicio - WorkerNationalIdentifier
							PersonNationalIdentifier personNationalIdentifier1 = new PersonNationalIdentifier();
							personNationalIdentifier1.setLegislationCode(rs.getString("codigo_legislacion"));
							personNationalIdentifier1.setIssueDate(DocumentUtil.getXMLGregorianCalendar("IssueDate", rs.getString("fecha_contratacion")));
							personNationalIdentifier1.setExpirationDate(DocumentUtil.getXMLGregorianCalendar("ExpirationDate", "4712-12-31"));
							personNationalIdentifier1.setPlaceOfIssue(DocumentUtil.getXMLString("PlaceOfIssue", rs.getString("codigo_legislacion")));
							personNationalIdentifier1.setNationalIdentifierType(rs.getString("tipo_identificador1"));
							personNationalIdentifier1.setNationalIdentifierNumber(rs.getString("numero_identificador1"));
							personNationalIdentifier1.setPrimaryFlag(DocumentUtil.getXMLBoolean("PrimaryFlag", true));
							
							PersonNationalIdentifier personNationalIdentifier2 = new PersonNationalIdentifier();
							personNationalIdentifier2.setLegislationCode(rs.getString("codigo_legislacion"));
							personNationalIdentifier2.setIssueDate(DocumentUtil.getXMLGregorianCalendar("IssueDate", rs.getString("fecha_contratacion")));
							personNationalIdentifier2.setExpirationDate(DocumentUtil.getXMLGregorianCalendar("ExpirationDate", "4712-12-31"));
							personNationalIdentifier2.setPlaceOfIssue(DocumentUtil.getXMLString("PlaceOfIssue", rs.getString("codigo_legislacion")));
							personNationalIdentifier2.setNationalIdentifierType(rs.getString("tipo_identificador2"));
							personNationalIdentifier2.setNationalIdentifierNumber(rs.getString("numero_identificador2"));
							personNationalIdentifier2.setPrimaryFlag(DocumentUtil.getXMLBoolean("PrimaryFlag", true));						
							
							
							w.getWorkerNationalIdentifier().add(personNationalIdentifier1);
							w.getWorkerNationalIdentifier().add(personNationalIdentifier2);
							//Final - WorkerNationalIdentifier
							
							//Inicio - WorkerDriversLicence
							PersonDriversLicence personDriversLicence1 = new PersonDriversLicence();
							personDriversLicence1.setLegislationCode(rs.getString("codigo_legislacion"));
							personDriversLicence1.setLicenseNumber(DocumentUtil.getXMLString("LicenseNumber", "A1"));
							personDriversLicence1.setDateFrom(DocumentUtil.getXMLGregorianCalendar("DateFrom", rs.getString("fecha_licencia1")));				
							DriversLicenseTypeDFF driversLicenseTypeDFF = new DriversLicenseTypeDFF();
							driversLicenseTypeDFF.setCategoria(DocumentUtil.getXMLStringDff("categoria", rs.getString("categoria_licencia1")));				
							personDriversLicence1.setDriversLicenseTypeDFF(driversLicenseTypeDFF);
							
							
							
							PersonDriversLicence personDriversLicence2 = new PersonDriversLicence();
							personDriversLicence2.setLegislationCode(rs.getString("codigo_legislacion"));
							personDriversLicence2.setLicenseNumber(DocumentUtil.getXMLString("LicenseNumber", "B1"));
							personDriversLicence2.setDateFrom(DocumentUtil.getXMLGregorianCalendar("DateFrom", rs.getString("fecha_licencia2")));				
							DriversLicenseTypeDFF driversLicenseTypeDFF2 = new DriversLicenseTypeDFF();
							driversLicenseTypeDFF2.setCategoria(DocumentUtil.getXMLStringDff("categoria", rs.getString("categoria_licencia2")));				
							personDriversLicence2.setDriversLicenseTypeDFF(driversLicenseTypeDFF2);
							
							
							PersonDriversLicence personDriversLicence3 = new PersonDriversLicence();
							personDriversLicence3.setLegislationCode(rs.getString("codigo_legislacion"));
							personDriversLicence3.setLicenseNumber(DocumentUtil.getXMLString("LicenseNumber", "C1"));
							personDriversLicence3.setDateFrom(DocumentUtil.getXMLGregorianCalendar("DateFrom", rs.getString("fecha_licencia3")));				
							DriversLicenseTypeDFF driversLicenseTypeDFF3 = new DriversLicenseTypeDFF();
							driversLicenseTypeDFF3.setCategoria(DocumentUtil.getXMLStringDff("categoria", rs.getString("categoria_licencia3")));				
							personDriversLicence3.setDriversLicenseTypeDFF(driversLicenseTypeDFF3);
							
							w.getWorkerDriversLicence().add(personDriversLicence1);
							w.getWorkerDriversLicence().add(personDriversLicence2);
							w.getWorkerDriversLicence().add(personDriversLicence3);
							//Final - WorkerDriversLicence
							
							//Inicio - WorkerUserInformation
							PersonUserInformation personUserInformation = new PersonUserInformation();
							personUserInformation.setUserName(DocumentUtil.getXMLString("UserName", rs.getString("usuario")));
							personUserInformation.setSendCredentialsEmailFlag(DocumentUtil.getXMLBoolean("SendCredentialsEmailFlag", Boolean.parseBoolean(rs.getString("envia_credenciales"))));
							personUserInformation.setEmailAddress(DocumentUtil.getXMLString("EmailAddress", rs.getString("correo_empresa")));
							w.setWorkerUserInformation(personUserInformation);
							//Final - WorkerUserInformation
							
										
							//ActionList
							al.setActionCode(DocumentUtil.getXMLString("ActionCode", rs.getString("accion")));
							
							LOGGER.info("Enviando datos al web service.");	
							LOGGER.info("### Ejecutando el metodo: createWorker");
							metodo = "createWorker";
							response.setResult(port.createWorker(w, al));							
							xmlGenerado1 = wsse.getXml_generated();
							
														
							if(response.getResult() != null && response.getResult().getValue().size() > 0){
								LOGGER.info("Obteniendo respuesta exitosa.");
								LOGGER.info("PersonId: "+response.getResult().getValue().get(0).getPersonId());
								
								//Realizar el update - un metodo
								int exito = updateResponseTable(id_number, "PersonId: "+response.getResult().getValue().get(0).getPersonId().toString(), "OK", metodo, xmlGenerado1, xmlGenerado2, xmlGenerado3);
										  								
								if(exito == 1){
									LOGGER.info("Datos actualizados correctamente en la base de datos.");
								}
	
								
							}
						
						
		   			}else{
		   					//ws de actualizacion
		   				
		   					LOGGER.info("Proceso de actualizaci�n de un trabajador");
		   					
							wsse = new WSSESOAPHandler();
							wsse.setWSSE(properties.getProperty("ws.user"), properties.getProperty("ws.password"));
							wsseHR = new WSSESOAPHandlerResolver(wsse);
							Service service = Service.create(new URL(properties.getProperty("ws.endpoint")), new QName(properties.getProperty("ws.qname"), properties.getProperty("ws.name")));
							service.setHandlerResolver(wsseHR);				
							WorkerService port = service.getPort(WorkerService.class);
							
							String respuesta = "";
							id_number = rs.getInt("id_number");	
							mergeResponse = new MergePersonResponse();
							Person attributeList = null;						
							attributeList = new Person();
							
							LOGGER.info("Obteniendo datos del trabajador: " +rs.getString("nombre"));
							
							//PersonNumber
							attributeList.setPersonNumber(DocumentUtil.getXMLString("PersonNumber", rs.getString("no_persona")));
													
							//PersonDetails							
							PersonDetails personDetails = new PersonDetails();
							personDetails.setRangeStartDate(DocumentUtil.getXMLGregorianCalendar("RangeStartDate", rs.getString("fecha_inicio")));
							personDetails.setBloodType((DocumentUtil.getXMLString("BloodType", rs.getString("tipo_sangre"))));
							personDetails.setDateOfBirth(DocumentUtil.getXMLGregorianCalendar("DateOfBirth", rs.getString("fecha_nacimiento")));
							attributeList.getPersonDetails().add(personDetails);
							
							//PersonLegislativeData
							PersonLegislativeData personLegislativeData = new PersonLegislativeData();
							personLegislativeData.setRangeStartDate(DocumentUtil.getXMLGregorianCalendar("RangeStartDate", rs.getString("fecha_inicio")));
							personLegislativeData.setLegislationCode(rs.getString("codigo_legislacion"));
							personLegislativeData.setMaritalStatus(DocumentUtil.getXMLString("MaritalStatus", rs.getString("estado_civil")));							
							attributeList.getPersonLegislativeData().add(personLegislativeData);
							
							//PersonName
							PersonName personName = new PersonName();
							personName.setRangeStartDate(DocumentUtil.getXMLGregorianCalendar("RangeStartDate", rs.getString("fecha_inicio")));
							personName.setNameType("GLOBAL");
							personName.setLegislationCode(DocumentUtil.getXMLString("LegislationCode", rs.getString("codigo_legislacion")));
							personName.setFirstName(DocumentUtil.getXMLString("FirstName", rs.getString("nombre")));
							personName.setMiddleNames(DocumentUtil.getXMLString("MiddleNames", rs.getString("segundo_nombre")));
							personName.setLastName(rs.getString("apellido_paterno"));
							personName.setNameInformation1(DocumentUtil.getXMLString("NameInformation1", rs.getString("apellido_materno")));
							personName.setTitle(DocumentUtil.getXMLString("Title", "MR."));							
							attributeList.getPersonName().add(personName);
							
							//PersonNationalIdentifier
							PersonNationalIdentifier personNationalIdentifier1 = new PersonNationalIdentifier();
							personNationalIdentifier1.setLegislationCode(rs.getString("codigo_legislacion"));
							personNationalIdentifier1.setIssueDate(DocumentUtil.getXMLGregorianCalendar("IssueDate", rs.getString("fecha_inicio")));
							personNationalIdentifier1.setExpirationDate(DocumentUtil.getXMLGregorianCalendar("ExpirationDate", "4712-12-31"));
							personNationalIdentifier1.setPlaceOfIssue(DocumentUtil.getXMLString("PlaceOfIssue", rs.getString("codigo_legislacion"))); 
							personNationalIdentifier1.setNationalIdentifierType(rs.getString("tipo_identificador1"));
							personNationalIdentifier1.setNationalIdentifierNumber(rs.getString("numero_identificador1"));
							personNationalIdentifier1.setPrimaryFlag(DocumentUtil.getXMLBoolean("PrimaryFlag", true));
							
							PersonNationalIdentifier personNationalIdentifier2 = new PersonNationalIdentifier();
							personNationalIdentifier2.setLegislationCode(rs.getString("codigo_legislacion"));
							personNationalIdentifier2.setIssueDate(DocumentUtil.getXMLGregorianCalendar("IssueDate", rs.getString("fecha_inicio")));
							personNationalIdentifier2.setExpirationDate(DocumentUtil.getXMLGregorianCalendar("ExpirationDate", "4712-12-31"));
							personNationalIdentifier2.setPlaceOfIssue(DocumentUtil.getXMLString("PlaceOfIssue", rs.getString("codigo_legislacion")));
							personNationalIdentifier2.setNationalIdentifierType(rs.getString("tipo_identificador2"));
							personNationalIdentifier2.setNationalIdentifierNumber(rs.getString("numero_identificador2"));
							personNationalIdentifier2.setPrimaryFlag(DocumentUtil.getXMLBoolean("PrimaryFlag", true));						
											
							attributeList.getPersonNationalIdentifier().add(personNationalIdentifier1);
							attributeList.getPersonNationalIdentifier().add(personNationalIdentifier2);
							
							//PersonDriversLicence
							PersonDriversLicence personDriversLicence1 = new PersonDriversLicence();
							personDriversLicence1.setLegislationCode(rs.getString("codigo_legislacion"));
							personDriversLicence1.setLicenseNumber(DocumentUtil.getXMLString("LicenseNumber", "A1"));
							personDriversLicence1.setDateFrom(DocumentUtil.getXMLGregorianCalendar("DateFrom", rs.getString("fecha_licencia1")));				
							DriversLicenseTypeDFF driversLicenseTypeDFF = new DriversLicenseTypeDFF();
							driversLicenseTypeDFF.setCategoria(DocumentUtil.getXMLStringDff("categoria", rs.getString("categoria_licencia1")));				
							personDriversLicence1.setDriversLicenseTypeDFF(driversLicenseTypeDFF);							
							
							
							PersonDriversLicence personDriversLicence2 = new PersonDriversLicence();
							personDriversLicence2.setLegislationCode(rs.getString("codigo_legislacion"));
							personDriversLicence2.setLicenseNumber(DocumentUtil.getXMLString("LicenseNumber", "B1"));
							personDriversLicence2.setDateFrom(DocumentUtil.getXMLGregorianCalendar("DateFrom", rs.getString("fecha_licencia2")));				
							DriversLicenseTypeDFF driversLicenseTypeDFF2 = new DriversLicenseTypeDFF();
							driversLicenseTypeDFF2.setCategoria(DocumentUtil.getXMLStringDff("categoria", rs.getString("categoria_licencia2")));				
							personDriversLicence2.setDriversLicenseTypeDFF(driversLicenseTypeDFF2);
							
							
							PersonDriversLicence personDriversLicence3 = new PersonDriversLicence();
							personDriversLicence3.setLegislationCode(rs.getString("codigo_legislacion"));
							personDriversLicence3.setLicenseNumber(DocumentUtil.getXMLString("LicenseNumber", "C1"));
							personDriversLicence3.setDateFrom(DocumentUtil.getXMLGregorianCalendar("DateFrom", rs.getString("fecha_licencia3")));				
							DriversLicenseTypeDFF driversLicenseTypeDFF3 = new DriversLicenseTypeDFF();
							driversLicenseTypeDFF3.setCategoria(DocumentUtil.getXMLStringDff("categoria", rs.getString("categoria_licencia3")));				
							personDriversLicence3.setDriversLicenseTypeDFF(driversLicenseTypeDFF3);
							
							attributeList.getPersonDriversLicence().add(personDriversLicence1);
							attributeList.getPersonDriversLicence().add(personDriversLicence2);
							attributeList.getPersonDriversLicence().add(personDriversLicence3);
							
							//PersonEmail
							//correo de trabajo
							PersonEmail personEmailTrabajo = new PersonEmail();
							personEmailTrabajo.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_inicio")));
							personEmailTrabajo.setEmailType("W1");
							personEmailTrabajo.setEmailAddress(rs.getString("correo_empresa"));
							
							//correo particular
							PersonEmail personEmailParticular = new PersonEmail();
							personEmailParticular.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_inicio")));
							personEmailParticular.setEmailType("H1");
							personEmailParticular.setEmailAddress(rs.getString("correo_personal"));
							
							attributeList.getPersonEmail().add(personEmailTrabajo);
							attributeList.getPersonEmail().add(personEmailParticular);
							
							//PersonAddress
							PersonAddress personAddress = new PersonAddress();
							personAddress.setRangeStartDate(DocumentUtil.getXMLGregorianCalendar("RangeStartDate", rs.getString("fecha_inicio")));
							personAddress.setAddressType(rs.getString("tipo_direccion"));
							personAddress.setCountry(rs.getString("pais"));
							personAddress.setAddressLine1(DocumentUtil.getXMLString("AddressLine1", rs.getString("direccion")));
							attributeList.getPersonAddress().add(personAddress);
							
							//PersonPhone
							PersonPhone personPhone1 = new PersonPhone();
							personPhone1.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_inicio")));
							personPhone1.setPhoneType("H1");
							personPhone1.setPhoneNumber(rs.getString("telefono_particular1"));
							
							PersonPhone personPhone2 = new PersonPhone();
							personPhone2.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_inicio")));
							personPhone2.setPhoneType("H2");
							personPhone2.setPhoneNumber(rs.getString("telefono_particular2"));
							
							PersonPhone personPhone3 = new PersonPhone();
							personPhone3.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_inicio")));
							personPhone3.setPhoneType("H3");
							personPhone3.setPhoneNumber(rs.getString("telefono_particular3"));
							
							PersonPhone personPhone4 = new PersonPhone();
							personPhone4.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_inicio")));
							personPhone4.setPhoneType("HM");
							personPhone4.setPhoneNumber(rs.getString("movil_particular1"));
							
							PersonPhone personPhone5 = new PersonPhone();
							personPhone5.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_inicio")));
							personPhone5.setPhoneType("HM2");
							personPhone5.setPhoneNumber(rs.getString("movil_particular2"));
							
							PersonPhone personPhone6 = new PersonPhone();
							personPhone6.setDateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar(rs.getString("fecha_inicio")));
							personPhone6.setPhoneType("HM3");
							personPhone6.setPhoneNumber(rs.getString("movil_particular3"));
									
							attributeList.getPersonPhone().add(personPhone1);
							attributeList.getPersonPhone().add(personPhone2);
							attributeList.getPersonPhone().add(personPhone3);
							attributeList.getPersonPhone().add(personPhone4);
							attributeList.getPersonPhone().add(personPhone5);
							attributeList.getPersonPhone().add(personPhone6);							
							
							
							metodo += "mergePerson";
							
							LOGGER.info("Enviando datos al web service.");
							LOGGER.info("### Ejecutando el metodo: mergePerson");
							
							mergeResponse.setResult(port.mergePerson(attributeList));														
												
							xmlGenerado1 =  wsse.getXml_generated();
							
							
							if(mergeResponse.getResult() != null && mergeResponse.getResult().getValue().size() > 0){
								LOGGER.info("Se ejecuto con exito el metodo");
								LOGGER.info("Obteniendo respuesta exitosa.");
								LOGGER.info("PersonId: "+mergeResponse.getResult().getValue().get(0).getPersonId());
								respuesta+= "PersonId: "+mergeResponse.getResult().getValue().get(0).getPersonId();
							}
							
							LOGGER.info("### Ejecutando el metodo: getWorkerInformationByPersonNumber");
							informationResponse = new GetWorkerInformationByPersonNumberResponse(); 	
							metodo += "/getWorkerInformationByPersonNumber";
							informationResponse.setResult(port.getWorkerInformationByPersonNumber(rs.getString("no_persona"), null));
							
							xmlGenerado2 =  wsse.getXml_generated();
							
							if(informationResponse.getResult() != null && informationResponse.getResult().getValue().size() > 0){
								LOGGER.info("Se ejecuto con exito el metodo");
								LOGGER.info("Obteniendo respuesta exitosa.");
								LOGGER.info("AssignmentId: "+informationResponse.getResult().getValue().get(0).getAssignmentId());
								respuesta+=" / AssignmentId: "+informationResponse.getResult().getValue().get(0).getAssignmentId();
							}
							
							LOGGER.info("### Ejecutando el metodo: updateAssignment");
							updateAssignmentResponse = new UpdateAssignmentResponse();
							
							al = new ActionsList();
							
							
							Assignment assignment = null;
							assignment = new Assignment();
							
							assignment.setAssignmentId(informationResponse.getResult().getValue().get(0).getAssignmentId());
							assignment.setPersonNumber(DocumentUtil.getXMLString("PersonNumber", rs.getString("no_persona")));
							assignment.setRangeStartDate(DocumentUtil.getXMLGregorianCalendar("RangeStartDate", rs.getString("fecha_inicio")));
							assignment.setBusinessUnitShortCode(DocumentUtil.getXMLString("BusinessUnitShortCode", rs.getString("unidad_negocio")));
							assignment.setPositionCode(DocumentUtil.getXMLString("PositionCode", rs.getString("codigo_posicion")));
							assignment.setDepartmentName(DocumentUtil.getXMLString("DepartmentName", rs.getString("departamento")));
							
							//BaseWorkerAsgDFF
							BaseWorkerAsgDFF baseWorkerAsgDFF = new BaseWorkerAsgDFF();
							baseWorkerAsgDFF.setBanco(DocumentUtil.getXMLStringBas("banco", rs.getString("nombre_banco")));
							baseWorkerAsgDFF.setCuenta(DocumentUtil.getXMLStringBas("cuenta", rs.getString("cuenta_bco")));
							baseWorkerAsgDFF.setTipoCuenta(DocumentUtil.getXMLStringBas("tipoCuenta", rs.getString("tipo_cuenta_bco")));
							baseWorkerAsgDFF.setCuentaCliente(DocumentUtil.getXMLStringBas("cuentaCliente", rs.getString("cuenta_cliente_bco")));
							baseWorkerAsgDFF.setCentroFuncionalDepartamento(DocumentUtil.getXMLStringBas("centroFuncionalDepartamento", rs.getString("centro_funcional_dep")));
							baseWorkerAsgDFF.setCentroFuncionalContable(DocumentUtil.getXMLStringBas("centroFuncionalContable", rs.getString("centro_funcional_cont")));
							assignment.setBaseWorkerAsgDFF(baseWorkerAsgDFF);
							
							//ActionList
							al.setActionCode(DocumentUtil.getXMLString("ActionCode", rs.getString("accion")));
							metodo += "/updateAssignment";
							updateAssignmentResponse.setResult(port.updateAssignment(assignment, al));
							xmlGenerado3 =  wsse.getXml_generated();
							
							if(updateAssignmentResponse.getResult() != null && updateAssignmentResponse.getResult().getValue().size() > 0){
								LOGGER.info("Se ejecuto con exito el metodo");
								LOGGER.info("Obteniendo respuesta exitosa.");
								LOGGER.info("Mensaje: Actualizacion de datos exitoso");
								respuesta+=" / Mensaje: Actualizacion de datos exitoso";
								
								int exito = updateResponseTable(id_number, respuesta, "OK", metodo, xmlGenerado1, xmlGenerado2, xmlGenerado3);
								if(exito == 1){
									LOGGER.info("Datos actualizados correctamente en la base de datos.");
								}
							}

						
						
		   			}
						
				
						
		   	}
		   	
		   	LOGGER.info("No hay trabajador que procesar ...");					
			Thread.sleep(2000);
			
		} 
		catch(WebServiceException wex){
			LOGGER.error("Error al acceder al WSDL " +properties.getProperty("ws.endpoint"));		
			wex.printStackTrace();
			
		}
		catch (Exception e) {			
			e.printStackTrace();
			
				if(wsse.getMessage() != null){	
					System.out.println(wsse.getXml_generated());
					LOGGER.error("\nException: "+wsse.getMessage());
					
					if(metodo.trim().equals("createWorker") || metodo.trim().equals("mergePerson")){
							xmlGenerado1 = xmlGenerado1 == null ? wsse.getXml_generated() : xmlGenerado1;
					}					
					else if(metodo.equals("mergePerson/getWorkerInformationByPersonNumber")){
							xmlGenerado2 = xmlGenerado2 == null ? wsse.getXml_generated() : xmlGenerado2;
					}
					//solo cuando trae null el metodo anterior
					else{
							xmlGenerado3 = xmlGenerado3 == null ? wsse.getXml_generated() : xmlGenerado3;
					}
					
					int exito = updateResponseTable(id_number, wsse.getMessage(), "ER", metodo, xmlGenerado1, xmlGenerado2, xmlGenerado3);
					if(exito == 1){
						LOGGER.info("Datos actualizados correctamente en la base de datos.");
					}
				}
		
			
		  	try {
	   			if (rs != null){rs.close();}
    			if (ps != null ) {ps.close();}	            
    			if (cn != null ) {cn.close();}
		  	} catch (SQLException ex) { 
				ex.printStackTrace();
		  	}
		}finally {
        	try {
        		if (rs != null) {rs.close();}
    			if (ps != null) {ps.close();}	            
    			if (cn != null) {cn.close();}
        	} catch (SQLException e) { 
				e.printStackTrace();
        	} 

	   }
		
		

	}	
	
	
	
	private int updateResponseTable(int id_number, String respuesta, String flag_status, String metodo, String xmlGenerado1, String xmlGenerado2, String xmlGenerado3){
		int exito = 0;
		try {
			
				LOGGER.info("Actualizando datos de respuesta del trabajor con id_number: "+id_number);
				String sqlUpdate = "update hcm_colaboradores set metodo_ws = ?, fecha_procesamiento = ?, respuesta_ws = ?, flag_status = ?, xml_enviado1 = ?, xml_enviado2 = ?, xml_enviado3 = ?, estado = 'CP' where id_number = ?";
				cn.setAutoCommit(false);
				ps = cn.prepareStatement(sqlUpdate);
				ps.setString(1, metodo);				
				ps.setTimestamp(2, new Timestamp(new LocalDateTime().toDateTime(dateTimeZone).getMillis()));
				ps.setString(3, respuesta);
				ps.setString(4, flag_status);
				ps.setString(5, xmlGenerado1);
				ps.setString(6, xmlGenerado2);
				ps.setString(7, xmlGenerado3);
				ps.setInt(8, id_number);
				exito = ps.executeUpdate();
				cn.commit();
			
		} catch (Exception e) {
			LOGGER.error("Error :", e);
			  if (cn != null) {
	                try {							                		
	                    	LOGGER.error("La transacci�n se deshace.");
	                    	cn.rollback();
	                } catch (SQLException excep) {
	                    	e.printStackTrace();
	                }
	          }
		}
		return exito;
	}
	

	private boolean initPropertyFile(){ 
		InputStream io = null;
		try
		{
			if(properties==null){
				properties = new Properties();
				io = new FileInputStream(System.getProperty("hcm.context")+"hcm.properties");
				properties.load(io);
			}
		}catch(IOException e){
			return false;
		}finally{
			try{
				if(io!=null){
					io.close();
				}	
			}catch(IOException e){
			}
		}
		return true;
	}
	
}